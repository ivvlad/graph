module.exports = {
  env: {
    browser: true,
    es2021: true,
  },
  extends: [
    'plugin:react/recommended',
    'airbnb',
  ],
  parserOptions: {
    ecmaFeatures: {
      jsx: true,
    },
    ecmaVersion: 12,
    sourceType: 'module',
  },
  plugins: [
    'react',
  ],
  rules: {
    'react/jsx-filename-extension': 'off',
    'react/prop-types': 'off',
    quotes: ['error', 'single'],
    semi: 'error',
    indent: ['error', 2],
    'no-var': 'error',
    'no-multi-spaces': 'error',
    'space-in-parens': 'error',
    'no-multiple-empty-lines': 'error',
    'prefer-const': 'error',
    'no-whitespace-before-property': 'error',
    'no-unused-vars': 'warn',
    'no-console': ['warn', { allow: ['warn'] }],
    'no-underscore-dangle': 'off',
  },
};
