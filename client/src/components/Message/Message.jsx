import React, { useState } from 'react';
import { Mutation } from 'react-apollo';

import styles from './message.module.css';
import like from './assets/like.svg';
import dislike from './assets/dislike.svg';
import checkMark from './assets/check-mark.svg';
import close from './assets/close.svg';

import {
  POST_REPLY_MUTATION, LIKE_MUTATION, DISLIKE_MUTATION,
} from '../../queries';
import { updateAftergReply } from '../../helpers/graphql';
import Reply from '../Reply/Reply';

const Message = ({ message }) => {
  const [isReply, setIsReply] = useState(false);
  const [replyMessage, setReplyMessage] = useState('');

  const {
    id, body, likes, dislikes,
  } = message;
  const userId = id.substr(-3);

  return (
    <>
      <li className={styles.wrapper}>
        <div className={styles.id}>
          #
          { userId }
        </div>
        <p>{ body }</p>
        {!isReply && (
          <button type="button" className={[styles.button, styles.reply].join(' ')} onClick={() => setIsReply(true)}>
            Reply
          </button>
        ) }
        <Mutation
          mutation={LIKE_MUTATION}
          variables={{ messageId: id }}
        >
          {(postMutation) => (
            <button type="button" className={[styles.button, styles.like].join(' ')} onClick={() => postMutation()}>
              { likes }
              <img src={like} alt="like button" />
            </button>
          )}
        </Mutation>
        <Mutation
          mutation={DISLIKE_MUTATION}
          variables={{ messageId: id }}
        >
          {(postMutation) => (
            <button type="button" className={[styles.button, styles.dislike].join(' ')} onClick={() => postMutation()}>
              { dislikes }
              <img src={dislike} alt="dislike button" />
            </button>
          )}
        </Mutation>
      </li>
      {message.replies && message.replies.map((reply) => <Reply key={reply.id} reply={reply} classList={[styles.wrapper, styles.replyBlock].join(' ')} />)}
      { isReply && (
        <Mutation
          mutation={POST_REPLY_MUTATION}
          variables={{ messageId: id, body: replyMessage }}
          update={(store, { data: { postReply } }) => {
            updateAftergReply(store, postReply, id);
          }}
          onCompleted={() => {
            setReplyMessage('');
            setIsReply(false);
          }}
        >
          {(postMutation) => (
            <div className={[styles.wrapper, styles.replyBlock].join(' ')}>
              <textarea type="text" placeholder="Reply" value={replyMessage} onChange={(ev) => setReplyMessage(ev.currentTarget.value)} />
              <button type="button" className={[styles.button, styles.checkMark].join(' ')} onClick={() => postMutation()}>
                <img src={checkMark} alt="checkMark button" />
              </button>
              <button type="button" className={[styles.button, styles.close].join(' ')} onClick={() => setIsReply(false)}>
                <img src={close} alt="close button" />
              </button>
            </div>
          )}
        </Mutation>
      ) }
    </>
  );
};

export default Message;
