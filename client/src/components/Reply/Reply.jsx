import React from 'react';

import styles from './reply.module.css';

const Reply = ({ reply, classList }) => (
  <div className={classList}>
    <div className={styles.id}>
      #
      { reply.id.substr(-3) }
    </div>
    <p>{ reply.body }</p>
  </div>
);

export default Reply;
