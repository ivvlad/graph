import React, { useState } from 'react';
import { Mutation } from 'react-apollo';

import styles from './createMessage.module.css';

import { POST_MESSAGE_MUTATION } from '../../queries';
import { updateAfterMessage } from '../../helpers/graphql';

const CreateMessage = () => {
  const [body, setBody] = useState('');

  return (
    <Mutation
      mutation={POST_MESSAGE_MUTATION}
      variables={{ body }}
      update={(store, { data: { postMessage } }) => {
        updateAfterMessage(store, postMessage);
      }}
      onCompleted={() => setBody('')}
    >
      {(postMutation) => (
        <div className={styles.wrapper}>
          <textarea
            className={styles.textArea}
            value={body}
            onChange={(el) => setBody(el.currentTarget.value)}
          />
          <button
            onClick={() => postMutation()}
            className={styles.button}
            type="button"
          >
            Send
          </button>
        </div>
      )}
    </Mutation>
  );
};

export default CreateMessage;
