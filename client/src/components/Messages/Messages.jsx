import React from 'react';
import { Query } from 'react-apollo';

import styles from './messages.module.css';
import CreateMessage from '../CreateMessage/CreateMessage';
import Message from '../Message/Message';

import { MESSAGES_QUERY } from '../../queries';
import { messageSubscribe, reactionSubscribe } from '../../helpers/graphql';

const Messages = () => (
  <Query query={MESSAGES_QUERY}>
    {({
      loading, error, data, subscribeToMore,
    }) => {
      if (loading) return <div>Loading...</div>;
      if (error) return <div>Error</div>;

      messageSubscribe(subscribeToMore);
      reactionSubscribe(subscribeToMore);

      const { messages: { messageList } } = data;
      return (
        <>
          <ul className={styles.wrapper}>
            {messageList.map((message) => <Message key={message.id} message={message} />)}
          </ul>
          <CreateMessage />
        </>
      );
    }}
  </Query>
);

export default Messages;
