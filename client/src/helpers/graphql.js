import { MESSAGES_QUERY, NEW_MESSAGE_REACTION_SUBSCRIPTION, NEW_MESSAGE_SUBSCRIPTION } from '../queries';

export const updateAfterMessage = (store, newMessage) => {
  const orderBy = 'createdAt_DESC';
  const data = store.readQuery({
    query: MESSAGES_QUERY,
    variables: {
      orderBy,
    },
  });
  data.messages.messageList.push(newMessage);
  store.writeQuery({
    query: MESSAGES_QUERY,
    data,
  });
};

export const updateAftergReply = (store, newReply, messageId) => {
  const orderBy = 'createdAt_DESC';
  const data = store.readQuery({
    query: MESSAGES_QUERY,
    variables: {
      orderBy,
    },
  });
  const repliedMessage = data.messages.messageList.find(
    (message) => message.id === messageId,
  );
  repliedMessage.replies.push(newReply);
  store.writeQuery({
    query: MESSAGES_QUERY,
    data,
  });
};

export const messageSubscribe = (subscribeToMore) => {
  subscribeToMore({
    document: NEW_MESSAGE_SUBSCRIPTION,
    updateQuery: (prev, { subscriptionData }) => {
      if (!subscriptionData.data) return prev;
      const { newMessage } = subscriptionData.data;
      const exists = prev.messages.messageList.find(({ id }) => id === newMessage.id);
      if (exists) return prev;

      return {
        ...prev,
        messages: {
          messageList: [...prev.messages.messageList, newMessage],
          count: prev.messages.messageList.length + 1,
          __typename: prev.messages.__typename,
        },
      };
    },
  });
};

export const reactionSubscribe = (subscribeToMore) => {
  subscribeToMore({
    document: NEW_MESSAGE_REACTION_SUBSCRIPTION,
  });
};
