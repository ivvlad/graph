import React from 'react';

import Messages from './components/Messages/Messages';

const App = () => (
  <div className="App">
    <Messages />
  </div>
);

export default App;
